import geopandas as gpd
if __name__=="__main__":
	density=gpd.read_file('torino.shp')
	areas=gpd.read_file('wetransfer_area1kw_2021-10-19_0726/area1kW/areasuit1kW.shp')

	density["ratioRoofVsDens"]=0

	ratioRoofVsDens=[]
	for i,r in density.iterrows():
		print(f"{i/density.shape[0]}%",end='\r')
		polygons=[]
		for j,s in areas.iterrows():
			if r.geometry.contains(s.geometry):
				polygons.append(s.geometry)
		ratioRoofVsDens.append(sum([x.area for x in polygons])*(3600/0.032)**2)

	density["ratioRoofVsDens"]=ratioRoofVsDens

	density.to_file('torino_processed.shp')

