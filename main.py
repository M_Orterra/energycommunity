from utils.savings import *
from utils.tiff import *
from utils.spots import *
from utils.placer import *
from utils.classic import *
from utils.tiff import *
from traces.mem_zonal_15 import *
from utils.eval import *
# from traces.mem_zonal_v2 import *
from traces import *
from utils.installation import *
import geopandas as gpd
from utils.simulation import *
import pandas as pd



def greedyAlgorithm(gh_t, rotationType=''):
    suitable_area = gpd.read_file(DATAFOLDER+'areasuit_w_height_roofID.shp')
    all_panels = detectAllSpots(suitable_area,rotationType)
    for t in gh_t:
        print(f"GHI threshold :{t}")
        panels_sorted = sortedZonalStats(all_panels, t,DATAFOLDER)
        output = findBestPlaces(all_panels, panels_sorted)
        output.to_file(RESULTFOLDER+"greedy_"+str(t)+"_"+rotationType+".shp")


def classicAlgorithm(gh_t, rotationType=''):
    suitable_area = DATAFOLDER+'areasuit_w_height_roofID.shp'
    for t in gh_t:
        output = RESULTFOLDER+"greedy_"+str(t)+"_"+rotationType+".shp"
        gdf = getClassic(suitable_area, output).to_file(
            RESULTFOLDER+"classic_"+str(t)+"_"+rotationType+".shp")
def clipPercentile(gh_t,rotationType=''):
    for t in gh_t:
        t=str(t)
        greedy=gpd.read_file(RESULTFOLDER+"greedy_"+t+"_"+rotationType+".shp").set_crs('epsg:4326')
        #classic=gpd.read_file(RESULTFOLDER+"classic_"+t+"_"+rotationType+".shp").set_crs('epsg:4326')
        points=gpd.read_file(DATAFOLDER+"percentile75.shp").set_crs('epsg:4326')
        greedy_clip=gpd.clip(points,greedy).to_crs(epsg=4326)
        #classic_clip=gpd.clip(points,classic).to_crs(epsg=4326)
        greedy_clip.to_file(POINTSFOLDER+"greedy_"+t+"_"+rotationType+"_points.shp")
        #classic_clip.to_file(POINTSFOLDER+"classic_"+t+"_"+rotationType+"_points.shp")

def evaluateTraces(filename):
    shapefile=POINTSFOLDER+filename+"_points.shp"
    radiation  = evaluate(shapefile,RASTERFOLDER)
    radiation.to_csv(TRACESFOLDER+filename+'.csv')  

# def evaluateTracesv2(filename):
#     shapefile="points2_shp/"+filename+"_points.shp"
#     radiation  = evaluate(shapefile)
#     radiation.to_csv("traces2/"+filename+'.csv')      


datafolder="data{}/"
resultfolder="resultv{}/"
pointsfolder="points{}_shp/"
tracesfolder="traces{}/"
productionfolder="production{}/"
rasterfolder='blockRaster{}/'
inputraster='/media/HD/panelJournalRaster/blockRaster{}'

if __name__ == "__main__":
    savedCo2=[]
    savedEnergy=[]
    for case in range(1,5):
        print(f"Case :{case}")
        DATAFOLDER=datafolder.format(case)
        RESULTFOLDER=resultfolder.format(case)
        POINTSFOLDER=pointsfolder.format(case)
        TRACESFOLDER=tracesfolder.format(case)
        PRODUCTIONFOLDER=productionfolder.format(case)
        RASTERFOLDER=rasterfolder.format(case)
        INPUTRASTER=inputraster.format(case)
 
        outputRaster=DATAFOLDER
        if glob.glob(DATAFOLDER+'percentile75.shp')==[]:
            createPercentileTiff(INPUTRASTER,outputRaster,75)
            #createPercentileTiff(inputRaster,outputRaster,75)
        #gh_t = [x for x in range(200,600,100)]
        gh_t=[100]
        #rotationTypes=["v","h","m"]
        rotationTypes=["m"]
        for t in gh_t:
            for rt in rotationTypes:
                # print("\n----------\nPlacement phase\n----------")
                # greedyAlgorithm(gh_t, rotationType=rt)
                # clipPercentile(gh_t,rt)
                # print(f"Finished rotation: {rt}\n")

                # print("\n----------\nGHI phase\n----------")
                # print(f"Starting GHI evaluation for th={t} and type={rt}")
                # evaluateTraces("greedy_"+str(t)+"_"+rt)
                # eval("greedy_"+str(t)+"_"+rt,TRACESFOLDER,RESULTFOLDER)
                # print("completed GHI evaluation")

                # print("\n----------\nProduction phase\n----------")         
                # print(f"Calculating configuration total production for:\nth: {t} - rotation: {rt}")
                # greedyFile=TRACESFOLDER+"greedy_"+str(t)+"_"+str(rt)+"_traces.csv"
                # systemProduction(greedyFile,rt,t,PRODUCTIONFOLDER)

                # print("\n----------\nSimulation phase\n----------")
                # sections=gpd.read_file(DATAFOLDER+"block.shp").SEZ2011.values
                # simulatedHouses=simulateSections(sections)
                # simulatedHouses.to_csv(DATAFOLDER+"simulatedHouses.csv")

                print("\n----------\nEcologic phase\n----------")
                production=pd.read_csv(PRODUCTIONFOLDER+str(t)+"_"+str(rt)+"_production.csv")
                consumption=pd.read_csv(DATAFOLDER+'simulatedHouses.csv')
                saved,perc=co2Saving(production,consumption)
                savedCo2.append({"id":case,"co2":saved,"perc":perc})
                noPv,savedEn,perc=energySaving(production,consumption)
                savedEnergy.append({"id":case,"kind":"consumption","energy":noPv})
                savedEnergy.append({"id":case,"kind":"production","energy":savedEn})
                print(f"Saved co2: {savedCo2[-1]['co2']}")
     
        print(f"Finished case {case}")
    pd.DataFrame(savedCo2).to_csv("savedCo2.csv")
    pd.DataFrame(savedEnergy).to_csv("savedEnergy.csv")

            


