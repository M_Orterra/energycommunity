import geopandas as gpd
import pandas as pd
from shapely.geometry import Point, Polygon
import sys

CONVERSIONFACTOR=0.032/3600

def eval(placement_type,tracesFolder,resultFolder):
    df=pd.read_csv(tracesFolder+placement_type+'.csv')
    df2=[]
    for column in df.columns[1:]:
        if any(pd.isna(df[column][3:-1])==False):
            df2.append(df[column].values)
    
    clean=pd.DataFrame(df2)
    new_col={}
    for i,x in enumerate(df["Unnamed: 0"].values):
        new_col[i]=x
    clean=clean.rename(columns=new_col)
    clean=clean.fillna(0)
    final_df=pd.DataFrame()
    geodf=gpd.read_file(resultFolder+placement_type+'.shp')
    stop=len(geodf)
    for i,panel in geodf.iterrows():
        datas=[]
        for j,data in clean.iterrows():
            point=Point(data["x"],data["y"])       
            if panel["geometry"].distance(point)<2*CONVERSIONFACTOR:
                datas.append(data[3:])
        try:        
            temp=[]
            row=None
            temp_len=len(datas[0])
            datas_len=len(datas)
            if datas_len>1:
                for k in range(len(datas[0])):
                    value=min([v[k] for v in datas])
                    temp.append(value)
                values=pd.Series(temp)
            else:
                values=datas[0]
            
            row=pd.concat([values,panel],ignore_index=True)
            final_df=final_df.append(row,ignore_index=True)
            print(f"{i*100/stop}-{i} out of {stop}",end="\r")
        except Exception as e:
            print(f"{i*100/stop}-{i} out of {stop} - discarded panel",end="\r")
    final_df.to_csv(tracesFolder+placement_type+'_traces.csv')
