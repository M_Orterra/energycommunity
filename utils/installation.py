import numpy as np
import pandas as pd

def chunk(seq, size):
    return (seq[pos:pos + size] for pos in range(0, len(seq), size))

def powerProduction(G):
    p00 =  45.43
    p10 =  -12.67
    p01 =  -0.08475
    p20 =  2.969
    p11 =  0.01869
    p02 =  4.913e-05
    p30 =  -0.27
    p21 =  -0.001918
    p12 =  -2.492e-06
    p40 =  0.01054;
    p31 =  0.0001177
    p22 =  1.972e-07
    p50 = -0.0001502
    p41 = -2.326e-06
    p32 = -5.239e-09
    v=[0.0*i for i in range(len(G))]
    i=[0.0*i for i in range(len(G))]
    p=[0.0*i for i in range(len(G))]
    for c,g in enumerate(G):
        v[c]=g*0.0006667+ 22.73
        V=g*0.0006667+ 22.73
        p[c]= p00 + p10 * V + p01 * g + p20 * (V**2) + p11 * V * g + p02 * (g**2) + p30 * (V**3)\
                        +p21 * (V**2) * g + p12 * V * (g**2) + p40 * (V**4) + p31 * (V**3) * g\
                        +p22 * (V**2) * g**2 + p50 * (V**5) + p41 * (V**4) * g\
                        +p32 * (V**3) * g**2
        i[c]=p[c]/V
    return v,i,p

def systemProduction(greedyFile,rotation,th,folder):
    df=pd.DataFrame({})
    output_traces=pd.read_csv(greedyFile)

    output_traces=output_traces.astype({'16076':'int32'})
    output_traces=output_traces.astype({'16078':'int32'})

    # classic_traces=pd.read_csv(classicFile)

    # classic_traces=classic_traces.astype({'16076':'int32'})
    # classic_traces=classic_traces.astype({'16077':'int32'})

    output_roof=np.sort(output_traces["16076"].unique())
    # classic_roof=np.sort(classic_traces["16077"].unique())


    

    V=np.array([[0.0*i for i in range(len(output_traces.iloc[0,1:16077]))] for r in range(len(output_roof))])
    I=np.array([[0.0*i for i in range(len(output_traces.iloc[0,1:16077]))] for r in range(len(output_roof))])
    Pgreedy=np.array([[0.0*i for i in range(len(output_traces.iloc[0,1:16077]))] for r in range(len(output_roof))])
    for i in range(len(output_roof)):
        print(f"{i} of {len(output_roof)}",end="\r")
        x=output_traces[output_traces["16076"]==output_roof[i]]
        output_series=[s for s in chunk([p.values for _,p in x.iloc[:,1:16077].iterrows()],8)]
        Vseries=np.array([[0.0*i for i in range(len(output_traces.iloc[0,1:16077]))] for serie in output_series])
        Iseries=np.array([[0.0*i for i in range(len(output_traces.iloc[0,1:16077]))] for serie in output_series])
        Pseries=np.array([[0.0*i for i in range(len(output_traces.iloc[0,1:16077]))] for serie in output_series])
        voltage=np.array([[0.0*i for i in range(len(output_traces.iloc[0,1:16077]))] for _ in range(8)])
        current=np.array([[0.0*i for i in range(len(output_traces.iloc[0,1:16077]))] for _ in range(8)])
        power=np.array([[0.0*i for i in range(len(output_traces.iloc[0,1:16077]))] for _ in range(8)])
        for indexS,serie in enumerate(output_series):
            for indexP,panel in enumerate(serie):
                voltage[indexP,:],current[indexP,:],power[indexP,:]=powerProduction(panel)
            Vseries[indexS,:]=[sum([voltage[m,n] for m in range(8)]) for n in range(16076)]
            Iseries[indexS,:]=[min([current[m,n] for m in range(8)]) for n in range(16076)]
            Pseries[indexS,:]=[Vseries[indexS,_]*Iseries[indexS,_] for _ in range(len(Vseries[indexS,:]))]
        V[i,:] = [min([Vseries[m,n] for m in range(len(output_series))]) for n in range(16076)]
        I[i,:] = [sum([Iseries[m,n] for m in range(len(output_series))]) for n in range(16076)]
        Pgreedy[i,:]=np.multiply(V[i,:],I[i,:]) 
    greedyProduction=sum(Pgreedy)
    # V=np.array([[0.0*i for i in range(len(classic_traces.iloc[0,1:16077]))] for r in range(len(classic_roof))])
    # I=np.array([[0.0*i for i in range(len(classic_traces.iloc[0,1:16077]))] for r in range(len(classic_roof))])
    # Pclassic=np.array([[0.0*i for i in range(len(classic_traces.iloc[0,1:16077]))] for r in range(len(classic_roof))])
    # for i in range(len(classic_roof)):
    #     print(f"{i} of {len(classic_roof)}",end="\r")
    #     y=classic_traces[classic_traces["16077"]==classic_roof[i]]
    #     output_series=[s for s in chunk([p.values for _,p in y.iloc[:,1:16077].iterrows()],8)]
    #     Vseries=np.array([[0.0*i for i in range(len(classic_traces.iloc[0,1:16077]))] for serie in output_series])
    #     Iseries=np.array([[0.0*i for i in range(len(classic_traces.iloc[0,1:16077]))] for serie in output_series])
    #     Pseries=np.array([[0.0*i for i in range(len(classic_traces.iloc[0,1:16077]))] for serie in output_series])
    #     voltage=np.array([[0.0*i for i in range(len(classic_traces.iloc[0,1:16077]))] for _ in range(8)])
    #     current=np.array([[0.0*i for i in range(len(classic_traces.iloc[0,1:16077]))] for _ in range(8)])
    #     power=np.array([[0.0*i for i in range(len(classic_traces.iloc[0,1:16077]))] for _ in range(8)])
    #     for indexS,serie in enumerate(output_series):
    #         for indexP,panel in enumerate(serie):
    #             voltage[indexP,:],current[indexP,:],power[indexP,:]=powerProduction(panel)
    #         Vseries[indexS,:]=[sum([voltage[m,n] for m in range(8)]) for n in range(16076)]
    #         Iseries[indexS,:]=[min([current[m,n] for m in range(8)]) for n in range(16076)]
    #         Pseries[indexS,:]=[Vseries[indexS,_]*Iseries[indexS,_] for _ in range(len(Vseries[indexS,:]))]
    #     V[i,:] = [min([Vseries[m,n] for m in range(len(output_series))]) for n in range(16076)]
    #     I[i,:] = [sum([Iseries[m,n] for m in range(len(output_series))]) for n in range(16076)]
    #     Pclassic[i,:]=np.multiply(V[i,:],I[i,:])  
    # classicProduction=sum(Pclassic)

    pd.DataFrame({"greedy_"+rotation:greedyProduction}).to_csv(folder+str(th)+"_"+rotation+"_production.csv")
