import pandas as pd
def co2Saving(prod,cons):
    """
    Calculate the CO2 savings from prod and consumption
    """
    prod["Unnamed: 0"]=pd.read_csv('data/greedy_500_v.csv')["Unnamed: 0"].values[3:]
    prod=prod.rename(columns={'Unnamed: 0': "date"})
    prod=prod.set_index("date")
    prod.index=pd.to_datetime(prod.index, utc=True)
    prod.index=prod.index.tz_convert("Europe/Rome")
    prod=prod.resample('1h').mean()


    nopvCo2=cons.sum().P*0.256

    saved=prod.sum().greedy_m*(0.256-0.058)
    return saved,100*saved/nopvCo2

def energySaving(prod,cons):
    """
    Calculate the energy savings from prod and consumption
    """
    prod["Unnamed: 0"]=pd.read_csv('data/greedy_500_v.csv')["Unnamed: 0"].values[3:]
    prod=prod.rename(columns={'Unnamed: 0': "date"})
    prod=prod.set_index("date")
    prod.index=pd.to_datetime(prod.index, utc=True)
    prod.index=prod.index.tz_convert("Europe/Rome")
    prod=prod.resample('1h').mean()

    nopvEnergy=cons.sum().P

    saved=prod.sum().greedy_m
    return nopvEnergy,saved,100*saved/nopvEnergy