import random
import pandas as pd
def simulateSections(secToSim):
    r1=pd.read_csv("data/R01_indicatori_2011_sezioni.csv",sep=';')
    dataForSec=r1[r1["SEZ2011"].isin(secToSim)]
    df=pd.read_csv('data/yearConsumption.csv')
    breaks=[0,1557686.4052777775,3358268.0472222217,4814554.156388889,6754832.44388889]  
    df["cat"]=pd.cut(df.Wh,bins=breaks,labels=range(1,5))
    familySizes=['PF3','PF4','PF5','PF6']
    fakeHouses={}
    for i,r in dataForSec.iterrows():
        c=1
        #print(f'{100*i/len(dataForSec)} %',end='\r')
        for size in familySizes:
            nOfFamilies=r[size]
            print(size,nOfFamilies)
            for x in range(nOfFamilies):
                building=random.choice(df[df.cat==c].buildingID.values)
                if fakeHouses.get(str(building),None):
                    fakeHouses[str(building)]+=1
                else:
                    fakeHouses[str(building)]=1
            c+=1
    temp=pd.read_csv('/media/HD/datasets/ned/datas/270.csv')
    temp.t=pd.to_datetime(temp.t)
    temp=temp.set_index('t').resample('1h').mean()
    ned=pd.DataFrame({'t':temp.index,'P':[0.0 for x in temp.P.values]})
    for k,v in fakeHouses.items():
        temp=pd.read_csv('/media/HD/datasets/ned/datas/'+k+'.csv')
        temp.t=pd.to_datetime(temp.t)
        temp=temp.set_index('t').resample('1h').mean()
        ned.P=ned.P.values+temp.P.values*v
    ned=ned.set_index('t')
    return ned